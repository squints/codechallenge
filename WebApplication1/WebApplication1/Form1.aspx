﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Form1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Styles/Site.css" rel="stylesheet" type="text/css" />
</head>
 <body class="bordered">
    <form id="form1" runat="server">
   
    
        <h1>Stack Overflow Challenge<br /> by Mitch McCarsky<br /></h1>
        <hr style="height: 2px" />
        
        
       <h2>High Score:</h2>
        <asp:Literal runat="server" ID="highScoreError" />
        <b><%= hsR.title %></b><i>Score: <%= hsR.score %></i><br /><br />
        <%= hsR.users %>
        <br /><b>Total Rep:</b><i> <%= hsR.totRep %></i>
       <hr />

       <h2>Latest Questions:</h2>
       <asp:Literal runat="server" ID="latestError" />
        <span id="list">
        <asp:Repeater ID="latest" runat="server">
        <ItemTemplate>
             
             
             <br />
            <b>#<%#Eval("number") %> - <%# Eval("link") %></b> <i>Score: <%# Eval("score") %> Answers: <%# Eval("ansCount") %></i><br />
            
            <div class="figure"><%# Eval("grav") %><p><%#Eval("owner") %></p></div>

            <br /><br />
            <div class="info">
            <b>Most recent badge:</b> <%# Eval("lastBdg") %><br />
            <b>Badges earned 6mo. ago:</b> <%#Eval("totBdg") %> <br />
            &emsp;[<span style="color:#E6B800">&#9679;</span> <%#Eval("gold") %> <span style="color:#C2C2A3">&#9679;</span> <%#Eval("silver") %> <span style="color:#996633">&#9679;</span> <%#Eval("bronze")%>]
            </div>
            <hr style="width:80%" />
        </ItemTemplate>
        </asp:Repeater>
       </span>
   
    </form>
</body>
</html>
