﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;

namespace WebApplication1
{

    public partial class WebForm1 : System.Web.UI.Page
    {
        public hsResults hsR;
        public List<quResults> quR;
        protected void Page_Load(object sender, EventArgs e)
        {

            var fetchO = new WebApplication1.fetchOverflow();
            if (fetchO.error == true) 
            {
                Response.Redirect("Whoops.htm");
            }
            else
            {
                this.hsR = fetchO.hsR;
                this.quR = fetchO.quR;

                latest.DataSource = quR;
                latest.DataBind();
            }

        }
    }

    public class fetchOverflow
    {
        public hsResults hsR;
        public List<quResults> quR;
        public bool error;

        public fetchOverflow()
        {
            //fetch methods return true if successful, false otherwise
            this.error = !fetchHighScore();
            this.error = !fetchUpdate();
        }


        public bool fetchHighScore()
        {
            
            hsResults hsR = new hsResults();
           
            var question = fetchJson("https://api.stackexchange.com/2.1/questions?pagesize=1&order=desc&sort=votes&site=stackoverflow&filter=!0*2GnI9uHpz(0");
            if (question == null)
                return false;

            hsR.title = question["items"][0]["title"];
            hsR.score = question["items"][0]["score"];
            var answer = fetchJson("https://api.stackexchange.com/2.1/questions/" + question["items"][0]["question_id"] + "/answers?order=desc&sort=activity&site=stackoverflow&filter=!)R0FR9BfUO9ikMVkh3Wtoqjf");
            if (answer == null)
                return false;

            long totalRep = 0;
            String users = "<table><tr><td><b>User</b></td><td><b>Reputation</b></td></tr>"; 
            foreach (var currentOwner in answer["items"])
            {
                //currentOwner contains the Dictionary<string,dynamic> of the owner for this question
                
                totalRep += currentOwner["owner"]["reputation"];
                users += "<tr><td>" + currentOwner["owner"]["display_name"] + "</td><td>" + currentOwner["owner"]["reputation"] + "</td></tr>";

            }
            hsR.totRep = totalRep;
            hsR.users = users + "</table>";
            this.hsR = hsR;
            return true;
        }


        public bool fetchUpdate()
        {
            
            int pagesize = 5; //change this value to get more results

            var question = fetchJson("https://api.stackexchange.com/2.1/questions?pagesize=" + 
                pagesize + "&order=desc&sort=activity&site=stackoverflow&filter=!17YvvVkYp8PPfbIYLy0EpMnpMgEj5.rPjoxYa-iTzdGDDj");
            if (question == null)
                return false;


            List<quResults> quR = new List<quResults>(); //local version of quR
            quResults resTemp; //object placeholder
            int number = 0;
            foreach (var i in question["items"])
            {
                //i is the current Dictionary<string,dynamic> that contains the info for the current question
                resTemp = new quResults();

                resTemp.number = ++number;
                resTemp.link = "<a href=\"" + i["link"] + "\">" + i["title"] + "</a>";
                resTemp.ansCount = i["answer_count"];
                resTemp.score = i["score"];
                resTemp.owner = i["owner"]["display_name"];
                resTemp.grav = "<img src=\"" + i["owner"]["profile_image"] + "\" width=75 height=75>";
                
                //fetches badges by rank
                var userrank = fetchJson("https://api.stackexchange.com/2.1/users/" 
                    + i["owner"]["user_id"] + "/badges?todate=" + getOldTime() + "&order=desc&sort=rank&site=stackoverflow&filter=!SrhTsM0ifx*iqVJI-Q");
                
                //fetches the one most recent badge
                var userlast = fetchJson("https://api.stackexchange.com/2.1/users/" 
                    + i["owner"]["user_id"] + "/badges?pagesize=1&order=desc&sort=awarded&site=stackoverflow&filter=!2.5jnV)w_9i7VUdjnIFm-");
                

                //filters were created by https://api.stackexchange.com/docs
                
                if (userlast == null || userrank == null)
                    return false;

                if (userlast["items"].Count <= 0 || userlast["items"][0]["name"] == "")
                {
                    //there are no badge items
                    resTemp.lastBdg = "(User has no badges. Oh well.)";

                }
                else
                {
                    resTemp.lastBdg = userlast["items"][0]["name"];

                    foreach (var j in userrank["items"])
                    {
                        if (j["rank"] == "gold")
                        { resTemp.gold++; resTemp.totBdg++; }
                        if (j["rank"] == "silver")
                        { resTemp.silver++; resTemp.totBdg++; }
                        if (j["rank"] == "bronze")
                        { resTemp.bronze++; resTemp.totBdg++; }

                    }
                }
                quR.Add(resTemp);
            }
            this.quR = quR;
            return true;
        }


        public Dictionary<string, dynamic> fetchJson(String url)
        {
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.AutomaticDecompression = DecompressionMethods.GZip; //apparently, all requests are gzipped. Thanks for the headsup, StackExchange. </sarcasm>
                req.Method = WebRequestMethods.Http.Get;
                req.Accept = "application/json; charset=utf-8";
                var res = (HttpWebResponse)req.GetResponse();
                StreamReader read = new StreamReader(res.GetResponseStream(), System.Text.Encoding.UTF8);
                string json = read.ReadToEnd();
                read.Close();
                res.Close();
                JavaScriptSerializer jss = new JavaScriptSerializer();
                return jss.Deserialize<Dictionary<string, dynamic>>(json);
            }
           catch (WebException e) //Most common error. If this happens, just stop
            {
                return null;
            } 

        }

        public long getOldTime()
        {
            DateTime ago = DateTime.Today.AddMonths(-6);
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc); //need to convert DateTime to unix epoch time for the API
            return Convert.ToInt64((ago - epoch).TotalSeconds);

        }

    }

    public class hsResults
    {

        public long score { get; set; }
        public long totRep { get; set; }
        public String users { get; set; }
        public String title { get; set; }

        public hsResults()
        {
            this.score = 0;
            this.title = "";
            this.totRep = 0;
            this.users = "";
        }

    }

    public class quResults
    {
        public long score { get; set; }

        public String link { get; set; }
        public String owner { get; set; }
        public String grav { get; set; }
        public String lastBdg { get; set; }

        public int gold { get; set; }
        public int silver { get; set; }
        public int bronze { get; set; }
        public int totBdg { get; set; }

        public int ansCount { get; set; }
        public int number { get; set; }

        public quResults()
        {
            this.link = "";
            this.owner = "";
            this.grav = "";
            this.lastBdg = "";

            this.gold = 0;
            this.silver = 0;
            this.bronze = 0;
            this.totBdg = 0;

            this.number = 0;
            this.ansCount = 0;
            this.score = 0;
        }
    }




}